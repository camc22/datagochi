"""
The main module this ties together a Kivy app composed of the various layouts as well as the util libs for creating the DataGochi Experience
"""

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock
from kivy.logger import Logger
from kivy.properties import (
    NumericProperty,
    StringProperty,
    BooleanProperty,
    DictProperty,
)
from kivy.utils import platform
import android
from android.permissions import request_permissions, Permission
from android.storage import primary_external_storage_path
from datagochi.aai import DataGochi
from datagochi.sensordata import *
from datagochi.sensors import *
from plyer import storagepath
from datagochi.ui.faces import Face
from kivy.utils import platform




class MyRoot(BoxLayout):
    """
    The Root layout for holding all child elements
    """

    enabled = BooleanProperty(False)
    logging = BooleanProperty(False)
    vals = list()

    def __init__(self):
        super(MyRoot, self).__init__()
        if platform == 'android':
            request_permissions([Permission.WRITE_EXTERNAL_STORAGE, Permission.READ_EXTERNAL_STORAGE])
        
        self.sm = SensorManager()

    def toggle_logging(self):
        """
        Toggle collection of sensor data
        """
        if not self.enabled:
            print("[*] enable first!")
            self.logging = False
            self.ids.btn.text = "Start Logging"
            return
        if not self.logging:
            Clock.schedule_interval(self.sense, 1/20.0)
            print("KAKA")
            Clock.schedule_interval(self.save, 6)
            print("KAKA2")
            # do stuff
            self.logging = True
            self.ids.btn.text = "Stop Logging"
        else:
            Clock.unschedule(self.sense)
            Clock.unschedule(self.save)
            # do stuff
            self.logging = False
            self.ids.btn.text = "Start Logging"

    def do_toggle(self):
        """
        asdf
        """
        if not self.enabled:
            self.sm.enable_all()
            self.ids.toggle.text = "Stop"
            self.enabled = True
        else:
            self.sm.disable_all()
            self.ids.toggle.text = "Start"
            self.enabled = False

    def sense(self, dt):
        """
        get sensor data and update view
        """
        d = self.sm.get_asdict()
        Logger.info(f"[main] got: {dt}")
        Logger.info("[main] got: {}".format(d))
        self.ids.accel.text = f"Accel: \n{str(d['accel'])}"
        self.ids.gyro.text = f"Gyro: \n{str(d['gyro'])}"
        self.ids.proximity.text = f"Prox: {str(d['proxim'])}"
        self.ids.grav.text = f"Grav: \n{str(d['gravity'])}"
        self.ids.baro.text = f"Baro: {str(d['baro'])}"
        self.ids.light.text = f"Light {str(d['light'])}"
        self.vals.append(d)

    def save(self, dt):
        '''
        save the list of values to file
        '''
        print("KAKA3")
        date_now = datetime.now()
        ts = f"{date_now.year}{date_now.month}{date_now.day}{date_now.hour}{date_now.minute}{date_now.second}"
        p = primary_external_storage_path()
        dest = os.path.join(p, f"{ts}_datagochi.csv")
        print("KAKA4")

        def magnitude(x, y, z):
            return math.sqrt((x ** 2 + y ** 2 + z ** 2))

        with open(dest, "w", newline='') as csvfile:
            fieldnames = ["gravity", "gyroscope", "accelerometer", "proximity", "barometer", "light"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            Logger.info(f"[save] writing to {dest}")

            for d in self.vals:
                row = dict()
                _g = d['gravity']
                row['gravity'] = magnitude(_g[0], _g[1], _g[2]) if _g is not None else 0
                _a = d['accel']
                row['accelerometer'] = magnitude(_a[0], _a[1], _a[2]) if _a is not None else 0
                _gy = d['gyro']
                row['gyroscope'] = magnitude(_gy[0], _gy[1], _gy[2]) if _gy is not None else 0
                row['proximity'] = 1 if d['proxim'] else 0
                row['barometer'] = d['baro'] if d['baro'] is not None else 0
                row['light'] = d['light'] if d['light'] is not None else 0

                writer.writerow(row)

            self.vals = list()




class DataGochiApp(App):
    """
    The DataGochi App

    This is used by Kivy in order to construct the whole app experience
    """

    def build(self):
        # return Label(text="DataGochi")
        return MyRoot()


if __name__ == "__main__":
    DataGochiApp().run()
