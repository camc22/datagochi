##
# DataGochi
#
# @file
# @version 0.1

debug:
	poetry run buildozer -v android debug

release:
	poetry run buildozer -v android release

run:
	poetry run buildozer -v android run

clean:
	poetry run buildozer -v android clean


# end
