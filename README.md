# datagochi


The little datagochi that acts on your sensor data


## Getting started

```bash
git clone https://gitlab.com/camc22/datagochi

cd datagochi

poetry shell
poetry install

buildozer -v android debug
```

## Architecture

## Developing

Deps needed:


e.g. on deb-based systems:


```bash
apt update 

sudo apt install -y git zip unzip openjdk-13-jdk python3-pip autoconf libtool pkg-config zlib1g-dev libncurses5-dev libncursesw5-dev libtinfo5 cmake libffi-dev libssl-dev
```



