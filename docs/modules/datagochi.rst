DataGochi
=========


How the core mechanics work.

The DataGochi is what we call AAI or artifical AI, as it isn't really an AI, it just tries to pretend to be a living entity.
Just like the original tamagotchi did.


Needs
-----


The core needs we have defined so far are:



Energy
......

The energy dictates when the DataGochi is supposed to sleep and whether it can perform certain actions.

Hunger
......

Hunger (for now) is equal to the battery status. It's as "hungry" as you're phone's accumulator is empty.

Fun
...

An artifical parameter, this gets affected by how much Exercise the DataGochi gets and how socialized it is.



Social
......

(For now) corresponds direct to the promixity sensors' data.
Get close to your phone, it will get more socialized.


Hygiene
.......

The DataGochi is able to "wash" itself in order to restore Hygiene.

For now there's no direct sensor affecting this.

TODO: maybe moisture? -> e.g. "When in bathroom?"


Exercise
........

The DataGochi is capable of working out, it will increase Exercise greatly, Fun a bit and decreases Energy
