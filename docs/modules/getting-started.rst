Getting Started
===============


Datagochi is a project for creating a tamagochi-like experience, but instead of user input it uses a smartphone's sensors in order to interact with the needs.


Quickstart
----------

To get going fast, you just have to execute a number of steps:

.. code:: shell

   git clone https://gitlab.com/camc22/datagochi
   cd datagochi

   poetry shell
   poetry install

   make

this will assemble a debug `.apk` for you to install on your Android smartphone.
