Core Concept
============


The core concept of this app is to tie together sensor data with artificial needs of Tamagotchi-like entity.




Inspirations
------------

We took inspiration from, (not ripping off) some other projects/ideas.

We took a great deal from the following.

Tamagotchi
..........

The whole father of ideas was of course the original tamagotchi.

Can't believe they're still making `them <https://tamagotchi.com/>`_


Maslow's hierarchy of needs
...........................

We took a look at Maslow's hierachy of needs, which states that basic needs are stacked atop each other hierarchically.

To state them (from most fundamental) to most luxurious/advanced:

1. physiological
2. safety
3. belonging and love
4. esteem
5. cognitive
6. aesthetic
7. self-actualization
8. transcendence

Please see `Wikipedia <https://en.wikipedia.org/wiki/Maslow%27s_hierarchy_of_needs>`_ for more information on that matter.


We decided against this approach, as it was too complex to map those complex needs of human beings against numerical sensor data.


The Sims (Electronic Arts)
..........................

For our endeavor it was much more appealing to use some level of abstraction that existed already.

And being at the 4th installment of the series, you can bet that TheSims' developers quite made up their mind on how to bind human needs against simple stats.

That simplification was really what we were looking for.

We stumbled upon a user's guide on how to get going in the game, basically offering guidance to new players.

So `This Guide <https://www.carls-sims-4-guide.com/tutorials/sims.php>`_ really confronted us with the core ideas and abstractions were already elaborated in a well established video game series.

To state the core needs, we have:

- Energy
- Hunger
- Bladder
- Fun
- Social
- Hygiene

We decided against Bladder and instead added Exercise, because those concepts were easier to associate with what a smartphone does.

We took nothing more from this project / video game installment than the core idea of what needs might be feasible to emulate.

pwnagotchi
..........


Lastly, there is `pwnagotchi <https://pwnagotchi.ai/>`_ .

A project that allows you to turn your Raspberry Pi Zero W combined with some accessoires into some wifi-penetrating AI/tamagochi.

Thing is here that this is a **REAL** AI. It's actually learning, can talk to others of its kind and will adjust its behaviour based on how well you do fullfilling its needs (pwning wifis, hence the name).


Please mind that we did not copy any of its source and just took inspiration of the "faces" approach, namely to display some cute ASCII/UTF-8 emoticons instead of developing a complex UI or animations or whatsoever.

We did not even copy their source code for using their faces, but instead used `this <https://github.com/dysfunc/ascii-emoji>`_ collection of emojis.
