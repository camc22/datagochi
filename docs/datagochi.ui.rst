datagochi.ui package
====================

Submodules
----------

datagochi.ui.facelayout module
------------------------------

.. automodule:: datagochi.ui.facelayout
   :members:
   :undoc-members:
   :show-inheritance:

datagochi.ui.faces module
-------------------------

.. automodule:: datagochi.ui.faces
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: datagochi.ui
   :members:
   :undoc-members:
   :show-inheritance:
