datagochi package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   datagochi.sensors
   datagochi.ui

Submodules
----------

datagochi.aai module
--------------------

.. automodule:: datagochi.aai
   :members:
   :undoc-members:
   :show-inheritance:

datagochi.sensordata module
---------------------------

.. automodule:: datagochi.sensordata
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: datagochi
   :members:
   :undoc-members:
   :show-inheritance:
