.. datagochi documentation master file, created by
   sphinx-quickstart on Mon Sep  5 12:26:21 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to datagochi's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/getting-started.rst
   modules/concept.rst
   modules/datagochi.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. toctree::
   :maxdepth: 2
   :caption: Datagochi Index

   modules.rst



