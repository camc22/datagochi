"""
The main datagochi module for handling the "AAI"

This module holds everything needed to create what we call an
"AAI" -> "artificial AI", as it's not actually an AI but just pretends
to be a living entity, just like a tamagochi does.
"""

import uuid


class DataGochi:
    """
    The main DataGochi class

    This class describes the actual DataGochi that will be used.

    :param id: The unique ID to distinguish between DataGochi instances, defaults to random uuid.uuid4()
    :type id: uuid
    :param name: The (human-readable) name of the DataGochi, defaults to ""
    :type name: str, optional
    :param HUNGER: The Stat for tracking Hunger
    :type HUNGER: int
    :param HYGIENE: The Stat for tracking Hygiene
    :type HYGIENE: int
    :param ENERGY: The Stat for tracking Energy
    :type ENERGY: int
    :param SOCIAL: The Stat for tracking Social activity
    :type SOCIAL: int
    :param FUN: The Stat for tracking Fun or satisfaction
    :type FUN: int
    :param EXERCISE: The Stat for tracking Exercise or personal fitness
    :type EXERCISE: int

    """

    id = None
    name = ""
    HUNGER = 0
    HYGIENE = 0
    ENERGY = 0
    SOCIAL = 0
    FUN = 0
    EXERCISE = 0

    def __init__(self, name=""):
        """
        Construct a DataGochi instance.

        By default the 'name' can be empty
        and a random uuid4 will be chosen for 'id'

        It initializes all needs with the value of 50
        """
        self.id = uuid.uuid4()
        self.name = name
        self.HUNGER = 50
        self.HYGIENE = 50
        self.ENERGY = 50
        self.SOCIAL = 50
        self.FUN = 50
        self.EXERCISE = 50

    def _get_stat(self, statname):
        # util function for getting a single stat
        stat = eval(f"self.{statname}")
        return stat

    def stats(self):
        """
        retrieve all stats in dict format

        :return: returs a dict of all the current stats
        :rtype: dict
        """
        d = {
            "HUNGER": self.HUNGER,
            "HYGIENE": self.HYGIENE,
            "ENERGY": self.ENERGY,
            "SOCIAL": self.SOCIAL,
            "FUN": self.FUN,
            "EXERCISE": self.EXERCISE,
        }

        return d

    def _affect_hunger(self, num):
        """
        Method for affecting the HUNGER stat.
        It takes care so that the value cannot leave the valid range from 0 to 100.

        :param num: integer value indicating how it should be affected. Positive values increase the stat, negative ones decrease it.
        """

        val = self.HUNGER + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.HUNGER = val

    def _affect_hygiene(self, num):
        """
        Method for affecting the HYGIENE stat.
        It takes care so that the value cannot leave the valid range from 0 to 100.

        :param num: integer value indicating how it should be affected. Positive values increase the stat, negative ones decrease it.
        """
        val = self.HYGIENE + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.HYGIENE = val

    def _affect_energy(self, num):
        """
        Method for affecting the ENERGY stat.
        It takes care so that the value cannot leave the valid range from 0 to 100.

        :param num: integer value indicating how it should be affected. Positive values increase the stat, negative ones decrease it.
        """
        val = self.ENERGY + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.ENERGY = val

    def _affect_social(self, num):
        """
        Method for affecting the SOCIAL stat.
        It takes care so that the value cannot leave the valid range from 0 to 100.

        :param num: integer value indicating how it should be affected. Positive values increase the stat, negative ones decrease it.
        """
        val = self.SOCIAL + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.SOCIAL = val

    def _affect_fun(self, num):
        """
        Method for affecting the FUN stat.
        It takes care so that the value cannot leave the valid range from 0 to 100.

        :param num: integer value indicating how it should be affected. Positive values increase the stat, negative ones decrease it.
        """
        val = self.FUN + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.FUN = val

    def _affect_exercise(self, num):
        """
        Method for affecting the EXERCISE stat.
        It takes care so that the value cannot leave the valid range from 0 to 100.

        :param num: integer value indicating how it should be affected. Positive values increase the stat, negative ones decrease it.
        """
        val = self.EXERCISE + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.EXERCISE = val

    def sleep(self):
        """
        Let the DataGochi sleep.

        (regenerates energy)
        """
        self._affect_energy(40)

    def eat(self):
        """
        Let the DataGochi eat.

        (positively affects Hunger)
        (regenerates energy)
        """
        self._affect_hunger(50)
        self._affect_energy(20)

    def work_out(self):
        """
        Let the DataGochi do a workout

        (costs energy)
        (increases fun)
        (increases exercise greatly)
        """
        self._affect_energy(-20)
        self._affect_fun(10)
        self._affect_exercise(50)

    def wash(self):
        """
        Let the DataGochi wash itself

        (greatly affects hygiene)
        """
        self._affect_hygiene(50)

    def play(self):
        """
        Let the DataGochi play

        (positively affects fun)
        """
        self._affect_fun(20)
