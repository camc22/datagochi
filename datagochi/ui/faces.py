"""
datagochi.faces

The faces the datagochi will display

the ASCII emojis are taken from:
https://github.com/dysfunc/ascii-emoji


TODO:

maybe use emojis from

https://en.wikipedia.org/wiki/List_of_emoticons

so that they're all displayable
"""
import random


class Face:
    """
    Class for holding the actual emoji faces
    """

    @classmethod
    def _facedict(cls):
        """
        masking this for documentation purposes.
        YOU'LL NEVER EVER GOING TO READ THIS!
        """

        facedict = {
            # "innocent_face": "ʘ‿ʘ",
            # "table_flip ": "(╯°□°）╯︵ ┻━┻",
            # "table_back": "┬─┬﻿ ノ( ゜-゜ノ)",
            # "tidy_up": "┬─┬⃰͡ (ᵔᵕᵔ͜ )",
            # "double_flip": "┻━┻ ︵ヽ(`Д´)ﾉ︵﻿ ┻━┻",
            # "fisticuffs": "ლ(｀ー´ლ)",
            # "cute_bear": "ʕ•ᴥ•ʔ",
            # "squinting_bear": "ʕᵔᴥᵔʔ",
            # "gtfo_bear": "ʕ •`ᴥ•´ʔ",
            # "cute_face": "(｡◕‿◕｡)",
            "shrug_face": "¯\_(ツ)_/¯",  # NICE
            "meh": "¯\(°_o)/¯",  # NICE
            # "feel_perky": "(`･ω･´)",
            # "excited": "☜(⌒▽⌒)☞",
            # "running": "ε=ε=ε=┌(;*´Д`)ﾉ",
            # "happy_face": "ヽ(´▽`)/",
            # "basking_in_glory": "ヽ(´ー｀)ノ",
            # "kitty_emote": "ᵒᴥᵒ#",
            # "fido": "V•ᴥ•V",
            # "meow": "ฅ^•ﻌ•^ฅ",
            # "cheers": "（ ^_^）o自自o（^_^ ）",
            "lenny": "( ͡° ͜ʖ ͡°)",  # NICE
            # "disagree": "٩◔̯◔۶",
            "flexing": "ᕙ(⇀‸↼‶)ᕗ",
            "do_you_even_lift_bro": "ᕦ(ò_óˇ)ᕤ",  # NICE
            "kirby": "⊂(◉‿◉)つ",
            "tripping_out": "q(❂‿❂)p",  # NICE
            # "discombobulated": "⊙﹏⊙",
            # "sad_and_confused": "¯\_(⊙︿⊙)_/¯",
            # "confused": "¿ⓧ_ⓧﮌ",
            # "confused_scratch": "(⊙.☉)7",
            # "worried": "(´･_･`)",
            # "dear_god_why": "щ（ﾟДﾟщ）",
            "staring": "٩(๏_๏)۶",
            # "strut": "ᕕ( ᐛ )ᕗ",
            "zoned": "(⊙_◎)",
            # "crazy": "ミ●﹏☉ミ",
            "fuck_it": "t(-_-t)",
            "hugger": "(づ￣ ³￣)づ",
            # "stranger_danger": "(づ｡◕‿‿◕｡)づ",
            # "cry_face": "｡ﾟ( ﾟஇ‸இﾟ)ﾟ｡",
            # "tgif": "“ヽ(´▽｀)ノ”",
            # "dancing": "┌(ㆆ㉨ㆆ)ʃ",
            # "sleepy": "눈_눈",
            # "shy": "(๑•́ ₃ •̀๑)",
            # "fly_away": "⁽⁽ଘ( ˊᵕˋ )ଓ⁾⁾",
            "careless": "◔_◔",
            "love": "♥‿♥",
            "touchy_feely": "ԅ(≖‿≖ԅ)",  # NICE
            "kissing": "( ˘ ³˘)♥",  # NICE
            "shark_face": "( ˇ෴ˇ )",
            "emo_dance": "ヾ(-_- )ゞ",
            # "dance": "♪♪ ヽ(ˇ∀ˇ )ゞ",
            # "opera": "ヾ(´〇`)ﾉ♪♪♪",
            # "winnie": "ʕ •́؈•̀ ₎",
            # "winnie2": "ʕ •́؈•̀)",
            "boxing": "ლ(•́•́ლ)",
            "fight": "(ง'̀-'́)ง",
            # "listening_to_headphones": "◖ᵔᴥᵔ◗ ♪ ♫",
            # "robot": "{•̃_•̃}",
            # "seal": "(ᵔᴥᵔ)",
            "questionable": "(Ծ‸ Ծ)",  # NICE
            "winning": "(•̀ᴗ•́)و ̑̑ ",
            # "zombie": "[¬º-°]¬",
            # "pointing": "(☞ﾟヮﾟ)☞",
            # "whistling": "(っ•́｡•́)♪♬",
            "injured": "(҂◡_◡)",  # NICE
            "creeper": "ƪ(ړײ)‎ƪ​",
            "eye_roll": "⥀.⥀",
            # "flying": "ح˚௰˚づ",
            "cant_be_unseen": "♨_♨",  # NICE
            "looking_down": "(._.)",
            # "im_a_hugger": "(⊃｡•́‿•̀｡)⊃",
            # "wizard": "(∩｀-´)⊃━☆ﾟ.*･｡ﾟ",
            # "yum": "(っ˘ڡ˘ς)",
            "hitchhiking": "(งツ)ว",
            "satisfied": "(◠﹏◠)",
            "sad_and_crying": "(ᵟຶ︵ ᵟຶ)",
            # "stunna_shades": "(っ▀¯▀)つ",
            # "barf": "(´ж｀ς)",
            "fuck_off": "(° ͜ʖ͡°)╭∩╮",  # NICE
            # "smiley_toast": "ʕʘ̅͜ʘ̅ʔ",
            # "exorcism": "ح(•̀ж•́)ง †",
            # "love": "-`ღ´-",
            # "taking_a_dump": "(⩾﹏⩽)",
            "wave_dance": "~(^-^)~",
            "happy_hug": "\(ᵔᵕᵔ)/",  # NICE
        }

        return facedict

    @classmethod
    def get_faces_avail(cls):
        """
        get a list of all faces available

        :return: all available face keys
        :rtype: list
        """
        return list(cls._facedict().keys())

    @classmethod
    def get_faces_dict(cls):
        """
        get the whole facedict

        :return: the dict of all the faces available, key being the description, value being the actual face as str
        :rtype: dict
        """
        return cls._facedict()

    @classmethod
    def rand_face(cls):
        """
        get a random face out of all the available ones

        :return: a random face from facedict
        :rtype: str
        """
        l = cls.get_faces_avail()
        i = random.randint(0, len(l) - 1)
        k = l[i]
        d = cls.get_faces_dict()
        f = d[k]
        return f

    @classmethod
    def get_face(cls, face="shrug_face"):
        """
        get a certain face out of all the available ones

        :param face: the key for retrieving from the facedict
        :type face: str, optional
        :return: a from facedict
        :rtype: str
        """
        d = cls.get_faces_dict()
        f = d[face]
        return f
