"""
Module for tying together faces in a Kivy-Layout
"""
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import StringProperty


class FaceLayout(BoxLayout):
    """
    The sub-layout responible for displaying a face
    """

    # is this needed?
    # face = StringProperty()

    def __init__(self):
        super(FaceLayout, self).__init__()
