"""
Module for providing unified, error-handled sensor access
"""

from kivy import Logger
from plyer import accelerometer
from plyer import barometer
from plyer import battery
from plyer import gravity
from plyer import gyroscope
from plyer import light
from plyer import proximity


class SensorManager:
    """
    The Core class for managing sensors and the device's sensor capabilities
    """

    sensors = list()

    def __init__(self):
        l = list()

        accel = Accel(accelerometer)
        gyro = Gyro(gyroscope)
        baro = Baro(barometer)
        bat = Bat(battery)
        g = Gravity(gravity)
        illum = Light(light)
        proxim = Proximity(proximity)

        l.append(accel)
        l.append(gyro)
        l.append(baro)
        l.append(bat)
        l.append(g)
        l.append(illum)
        l.append(proxim)

        self.sensors = l

    def available(self):
        return [s for s in self.sensors if s.available]

    def enabled(self):
        return [s for s in self.sensors if s.enabled]

    def disabled(self):
        return [s for s in self.sensors if not s.enabled]

    def enable_all(self):
        _ = [s.enable() for s in self.sensors]

    def disable_all(self):
        _ = [s.disable() for s in self.sensors if self.enabled]

    def get_all(self):
        return [s.safe_get() for s in self.sensors]

    def get_asdict(self):
        vals = [s.safe_get() for s in self.sensors]
        d = dict()
        d["accel"] = vals[0]
        d["gyro"] = vals[1]
        d["baro"] = vals[2]
        d["bat"] = vals[3]
        d["gravity"] = vals[4]
        d["light"] = vals[5]
        d["proxim"] = vals[6]
        return d


class BaseSensor:
    """
    The basic parent class that just defines how a sensor should behave
    """

    available = False
    enabled = False
    sensor = None

    def __init__(self):
        """
        default constructor
        """
        self.available = False
        self.enabled = False
        self.sensor = None

    def enable(self):
        """
        try to enable the sensor
        """
        raise NotImplementedError

    def disable(self):
        """
        try to disable the sensor
        """
        raise NotImplementedError

    def status(self):
        """
        get sensor status
        """
        if self.available:
            return self.enabled
        return False

    def get(self):
        """
        try to get the sensor data
        """
        raise NotImplementedError


class ConreteSensor(BaseSensor):
    """
    Sensor Data handling
    """

    def __init__(self, sensor):
        super(BaseSensor, self).__init__()
        self.sensor = sensor

    def enable(self):
        """
        enable gyroscope sensor
        """
        try:
            self.sensor.enable()
            # gyroscope.enable()
            self.available = True
            self.enabled = True
            Logger.info(f"{__name__}: enabling sensor")

        except:
            Logger.info(f"{__name__}: ERROR: can't enable sensor")

    def disable(self):
        """disable gyroscope sensor"""
        if self.available:
            if self.enabled:
                try:
                    # gyroscope.disable()
                    self.sensor.disable()
                    self.available = False
                    self.enabled = False
                    Logger.info(f"{__name__}: disabling sensor")
                except:
                    Logger.info(f"{__name__}: ERROR unable to disable sensor")

        else:
            Logger.info(f"{__name__}: ERROR: sensor not available")

    def get(self):
        """
        read sensor values

        NOTE: needs to be implemented for each specific sensor as the values differ
        """
        pass

    def safe_get(self):
        """
        try to safely get the sensor's value

        """
        try:
            return self.get()
        except:
            Logger.info(f"{__name__}: ERROR: could not get sensor data")
            return None


class Accel(ConreteSensor):
    """
    Accelerometer Sensor handling
    """

    def get(self):
        """
        read gyro sensor values

        :return: the acceleration sensor data (x,y,z), defaults to (0.0, 0.0, 0.0)
        :rtype: tuple
        """
        if self.available:
            if self.enabled:
                return self.sensor.acceleration
        return (0.0, 0.0, 0.0)


class Gyro(ConreteSensor):
    """
    Gyroscope Sensor handling
    """

    def get(self):
        """
        read gyro sensor values

        :return: the gyroscope sensor data (x,y,z), defaults to (0.0, 0.0, 0.0)
        :rtype: tuple
        """
        if self.available:
            if self.enabled:
                return self.sensor.rotation
        return (0.0, 0.0, 0.0)


class Baro(ConreteSensor):
    """
    Barometer Sensor handling
    """

    def get(self):
        """
        read gyro sensor values

        :return: the barometer sensor data in hPa, defaults to 0.0
        :rtype: float
        """
        if self.available:
            if self.enabled:
                return self.sensor.pressure
        return 0.0


class Bat(ConreteSensor):
    """
    Battery Sensor handling
    """

    def get(self):
        """
        read battery sensor values

        :return: the charging status os the device, defaults to {'isCharging': False, 'percentage': 0}
        :rtype: dict
        """
        if self.available:
            if self.enabled:
                return self.sensor.status
        return {"isCharging": False, "percentage": 0}


class Gravity(ConreteSensor):
    """
    Gravity Sensor handling
    """

    def get(self):
        """
        read gyro sensor values

        :return: the gravity sensor data (x,y,z), defaults to (0.0, 0.0, 0.0)
        :rtype: tuple
        """
        if self.available:
            if self.enabled:
                return self.sensor.gravity
        return (0.0, 0.0, 0.0)


class Light(ConreteSensor):
    """
    Light Sensor handling
    """

    def get(self):
        """
        read light sensor values

        :return: the light sensor data in lx, defaults to 0
        :rtype: int
        """
        if self.available:
            if self.enabled:
                return self.sensor.illumination
        return 0


class Proximity(ConreteSensor):
    """
    Proximity Sensor handling
    """

    def get(self):
        """
        read proximity sensor values

        :return: the proximity sensor data as bool, defaults to False
        :rtype: bool
        """
        if self.available:
            if self.enabled:
                return self.sensor.proximity
        return False
