"""

sensordata -- util class for processing and persisting sensor data in a unified way


NOTE: this module is used for doing some very basic ML stuff inside our DataGochi app



REVIEW: maybe we should implement this conditionally,
        so that even if numpy is not importable, we're
        just falling back to native python collections. lists?


SensorData to be processed:

- GyroX Float
- GyroY Float
- GyroZ Float
- Proximity Bool
- Barometer Float
- Bat Float
- Bat Load Bool
- Light Int
- Humidity Float
- Temp Float

"""
import csv
import json
import math
import os

from datetime import datetime


class XYZData:
    """
    A general class for handling XYZ-based sensordata

    :param x: the X axis value
    :type x: float
    :param y: the Y axis value
    :type y: float
    :param z: the Z axis value
    :type z: float
    """

    x = 0.0
    y = 0.0
    z = 0.0

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def magnitude(self):
        """
        Calculate the magnitude of this sensor

        :return: the magnitude of this sensor
        :rtype: float
        """
        return math.sqrt(self.x**2 + self.y**2 + self.z**2)

    def get_vals(self):
        """
        Return the values of the sensor

        :return: X, Y and Z data of this sensor
        :rtype: tuple
        """
        return (self.x, self.y, self.z)


class GyroData(XYZData):
    """
    class for handling Gyroscope data
    """

    pass


class SensorDataRow:
    """
    A wrapper around a single data entry of sensor data

    :param gyro: the Gyroscope data to add
    :type gyro: GyroData
    :param proxim: Proximity sensor data
    :type proxim: bool
    :param baro: Barometer sensor data
    :type baro: float
    :param bat: Battery sensor data
    :type bat: dict
    :param light: Light sensor data
    :type light: int
    :param humidity: Humidity sensor data
    :type humidity: float
    :param temp: Temperature sensor data in Celsius
    :type temp: float
    """

    gyro = None
    proxim = False
    baro = 0.0
    bat = {
        "charging": False,
        "capacity": 0.0,
    }
    light = 0
    humidty = 0.0
    temp = 0.0

    def __init__(self, gyro, proxim, baro, bat, light, humidty, temp):
        self.gyro = gyro
        self.proxim = proxim
        self.baro = baro
        self.bat = bat
        self.light = light
        self.humidty = humidty
        self.temp = temp

    def to_obj_dict(self):
        """
        return the attributes in a dictionary containing objects

        :return: the dictionary with objects nested in it
        :rtype: dict
        """
        d = {
            "gyroscope": self.gyro,
            "proximity": self.proxim,
            "barometer": self.baro,
            "battery": self.bat,
            "light": self.light,
            "humidity": self.humidity,
            "temp": self.temp,
        }
        return d

    def to_num_dict(self):
        """
        return the attributes in a flat (numerical) dict

        :return: the dictionary with flat float-based values
        :rtype: dict
        """
        d = {
            "gyro_x": self.gyro.x,
            "gyro_y": self.gyro.y,
            "gyro_z": self.gyro.z,
            "proximity": 0.0,
            "barometer": self.baro,
            "battery_capacity": self.bat["capacity"],
            "battery_charging": 0.0,
            "light": float(self.light),
            "humidity": self.humidity,
            "temp": self.temp,
        }
        if self.proxim == True:
            d["promixity"] = 1.0
        if self.bat["charging"] == True:
            d["battery_charging"] = 1.0

        return d

    def to_num_list(self):
        """
        return the values in a numerical list

        :return: a list with all numerical data in it
        :rtype: list
        """
        proximity = 1.0 if self.proxim else 0.0
        bat_charging = 1.0 if self.bat["charging"] else 0.0
        light = float(self.light)

        l = [
            self.gyro.x,
            self.gyro.y,
            self.gyro.z,
            proximity,
            self.baro,
            self.bat["capacity"],
            bat_charging,
            light,
            self.humidty,
            self.temp,
        ]

        return l


class SensorDataSeries:

    """
    SensorDataSeries -- util class for processing and storing sensor data in a unified way.

    :param data: the list of sensor data values
    :type data: list

    """

    data = list()

    def __init__(self, data=[]):
        data = data

    def add_row(self, row):
        """
        add a row from a preconstructed row

        :param row: the row to insert
        :type row: SensorDataRow
        """
        s = row
        self.data.append(s)

    def add(self, gyro, proxim, baro, bat, light, humidity, temp):
        """
        add objects to construct a SensorDataRow, then insert it

        :param gyro: holding gyroscope data
        :type gyro: GyroData
        :param proxim: whether proximity sensor detects something
        :type proxim: bool
        :param baro: barometer sensor data
        :type baro: float
        :param bat: holding battery information
        :type bat: dict
        :param light: light detection sensor data
        :type light: int
        :param humidity: humidity sensor data
        :type humidity: float
        """
        s = SensorDataRow(gyro, proxim, baro, bat, light, humidity, temp)
        self.data.append(s)

    def add_list(self, l):
        """
        add data from a list of values

        :param l: the numeric list of values to be added
        :type l: list
        """
        self.add_vals(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9])

    def add_vals(
        self, x, y, z, proxim, baro, bat_cap, bat_charge, light, humidity, temp
    ):
        """
        construct an entry from raw primitive data types


        :param x: the X value from the gyro sensor data
        :type x: float
        :param y: the Y value from the gyro sensor data
        :type y: float
        :param z: the Z value from the gyro sensor data
        :type z: float
        :param proxim: whether proximity sensor detects something
        :type proxim: bool
        :param baro: barometer sensor data
        :type baro: float
        :param bat_cap: the current battery capacity
        :type bat_cap: float
        :param bat_charge: whether the battery is charging
        :type bat_charge: bool
        :param light: light detection sensor data
        :type light: int
        :param humidity: humidity sensor data
        :type humidity: float
        """
        g = GyroData(x, y, z)
        bat = {
            "charging": bat_charge,
            "capacity": bat_cap,
        }
        r = SensorDataRow(g, proxim, baro, bat, light, humidity, temp)
        self.data.append(r)

    def dbg_dump(self):
        """
        print out the contens of the 'data' attribute (useful for debugging)
        """
        for r in self.data:
            print(r.to_num_list())

    def to_list_array(self):
        """
        turn the content of the data attribute into a list of lists

        :return: 2 dimensional array of the values
        :rtype: list
        """
        l = list()
        for r in self.data:
            l.append(r.to_num_list())
        return l

    def to_json(self):
        """
        serialize the 2d array of lists into JSON

        :return: JSON string of the numerical sensor data
        :rtype: str
        """
        l = self.to_list_array()
        j = json.dumps(l)
        return j

    def from_json(self, j):
        """
        restore data from a JSON-based string

        :param j: the JSON string to be loaded
        :type: j str
        """
        s = json.loads(j)

        for row in s:
            self.add_list(row)

    # DEPRECATED
    def Export(self, path):
        d = datetime.now()
        fname = f"{d.year}{d.month}{d.day}{d.hour}{d.minute}{d.second}_datagochi.json"
        doc = open(f"{path}/Log.txt", "w")
        doc.write(fname)
        doc.close()

    def export_csv(self, path=""):
        """
        export to the specified csv file
        """
        if path == "":
            p = os.path.abspath(os.path.curdir)
        else:
            p = os.path.abspath(path)
        d = datetime.now()
        fname = f"{d.year}{d.month}{d.day}{d.hour}{d.minute}{d.second}_datagochi.csv"
        dest = os.path.join(p, fname)

        with open(dest, "w", newline="") as csvfile:
            writer = csv.writer(csvfile, delimiter=",")
            print("[DBG] initialized CSV Writer for {}".format(fname))
            l = self.to_list_array()

            print("[DBG] gotta have {} data rows to write into CSV file".format(len(l)))

            for row in l:
                writer.writerow(row)

        print("[*] done writing {}".format(fname))

    def persist(self, path=""):
        """
        persist the data to file

        :param path: custom path where to safe the file
        :type path: str, optional

        TODO: check whether this method of saving sensor data is applicable for Android-based systems
        """
        if path == "":
            p = os.path.abspath(os.path.curdir)
        else:
            p = os.path.abspath(path)

        d = datetime.now()
        fname = f"{d.year}{d.month}{d.day}{d.hour}{d.minute}{d.second}_datagochi.json"
        dest = os.path.join(p, fname)

        with open(dest, "w") as f:
            j = self.to_json()
            f.write(j)

        print("[*] saved data to f{dest}")

    def restore(self, fname, path=""):
        """
        restore data from a given file

        :param fname: the filename to restore
        :type fname: str
        :param path: the path where to restore from (defaults to current dir)
        :type path: str, optional
        """
        if path == "":
            p = os.path.abspath(os.path.curdir)
        else:
            p = os.path.abspath(path)

        dest = os.path.join(p, fname)
        j = ""
        with open(dest, "r") as f:
            j = f.read()

            self.from_json(j)

    def flush(self):
        """
        flush the data
        """
        self.data = list()
