"""
The main datagochi module for handling the "AAI"
"""

import uuid

# Potential Needs (taken from Sims)
#
# Hunger
# Hygiene
# Energy
# Social
# Fun


class DataGochi:
    """
    The main DataGochi class
    """

    id = None
    name = ""
    HUNGER = 0#
    HYGIENE = 0
    ENERGY = 0#
    SOCIAL = 0#
    FUN = 0
    EXERCISE = 0

    def __init__(self, name=""):
        self.id = uuid.uuid4()
        self.name = name
        self.HUNGER = 50
        self.HYGIENE = 50
        self.ENERGY = 50
        self.SOCIAL = 50
        self.FUN = 50
        self.EXERCISE = 50

    def _get_stat(self, statname):
        stat = eval(f"self.{statname}")
        name =statname
        return statname+str(stat)

    def stats(self):
        """
        retrieve all stats in dict format
        """
        d = {
            "HUNGER": self.HUNGER,
            "HYGIENE": self.HYGIENE,
            "ENERGY": self.ENERGY,
            "SOCIAL": self.SOCIAL,
            "FUN": self.FUN,
            "EXERCISE": self.EXERCISE,
        }

        return d

    def _affect_hunger(self, num):

        #val = self.HUNGER + num
        val=num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.HUNGER = val

    def _affect_hygiene(self, num):
        val = self.HYGIENE + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.HYGIENE = val

    def _affect_energy(self, num):
        val = self.ENERGY + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.ENERGY = val

    def _affect_social(self, num):
        val = self.SOCIAL + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.SOCIAL = val

    def _affect_fun(self, num):
        val = self.FUN + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.FUN = val

    def _affect_exercise(self, num):
        val = self.EXERCISE + num

        if val < 0:
            val = 0
        elif val > 100:
            val = 100

        self.EXERCISE = val

    def sleep(self):
        self._affect_energy(40)

    def eat(self):
        self._affect_hunger(50)
        self._affect_energy(20)

    def work_out(self):
        self._affect_energy(-10)
        self._affect_fun(10)
        self._affect_exercise(20)

    def resting(self):
        self._affect_energy(3)
        self._affect_fun(2)
        self._affect_exercise(-6)

    def wash(self):
        self._affect_hygiene(50)

    def play(self):
        self._affect_fun(20)

    def socializing(self):
        self._affect_social(11)

    def not_socializing(self):
        self._affect_social(-3)
    
    def hunger_stat(self,load):
        self._affect_hunger(load)

    def wheather(self,num):
        self._affect_fun(num)

    def dark(self):
        self._affect_energy(-3)
    
    def light(self):
        self._affect_energy(+3)


    # TODO:
    # sleep
    # eat
    # work_out
    # wash
